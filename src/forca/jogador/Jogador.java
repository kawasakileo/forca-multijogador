package forca.jogador;

public class Jogador {
    int id;
    int pontuacao;
    String nome;

    public Jogador(int id, int pontuacao, String nome) {
        this.id = id;
        this.pontuacao = pontuacao;
        this.nome = nome;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPontuacao() {
        return pontuacao;
    }

    public void setPontuacao(int pontuacao) {
        this.pontuacao = pontuacao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
