package forca.servidor;

import forca.jogador.Jogador;
import leitor.Leitor;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Servidor {

    private static final int PONTUACAO_MAXIMA = 500;

    private static int pontuacaoRodada;

    private static String letrasUsadas = "";

    private static final Leitor leitor = new Leitor();

    private static final List<Jogador> listaJogadores = new ArrayList<>();

    private static List<String> listaPalavras = new ArrayList<>();

    private static List<String> listaLetrasCertas = new ArrayList<>();

    private static BufferedReader entradaSocket = null;

    private static PrintStream saidaSocket = null;

    private static boolean jogando;

    private static int contadorId = 0;

    private static int quantidadeDeJogadores = 1;

    private static ServerSocket serverSocket = null;

    private static Socket socket = null;

    public static void main(String[] args) {
        try {
            serverSocket = new ServerSocket(7777);
            System.out.println("Servidor ouvindo a porta 7777");
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            jogando = !serverSocket.isClosed();
            while (jogando) {
                socket = serverSocket.accept();

                contadorId = contadorId + 1;

                PrintWriter sermao = new PrintWriter(socket.getOutputStream());
                sermao.println("Digite seu nickname: ");
                sermao.flush();

                BufferedReader todoOuvidos = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                String nickname = todoOuvidos.readLine();

                System.out.println("Cliente conectado: " + socket.getInetAddress().getHostAddress()
                    + " | id: " + contadorId  + " | nickname: " + nickname);

                Jogador jogador = new Jogador(contadorId, 0, nickname);
                listaJogadores.add(jogador);

                if (listaJogadores.size() == quantidadeDeJogadores) {
                    inicioJogo();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void inicioJogo() throws IOException {
        System.out.println("*****Bem vindo ao jogo da Forca!*****");
        System.out.println();
        System.out.println("Dica: Frutas");
        leitor.lerArquivo("src/arquivos/banco-de-palavras.txt");
        listaPalavras = leitor.verificaPalavras();
        leitor.printPalavrasSombreadas(listaPalavras);

        // Cola para saber a palavra:
        System.out.println(listaPalavras);

        for (Jogador jogador : listaJogadores) {
            while (jogador.getPontuacao() < PONTUACAO_MAXIMA) {
                jogada(jogador);
            }
        }
    }

    public static void jogada(Jogador jogador) throws IOException {
        gerarPontuacaoRodada();

        printaRanking();

        PrintWriter mensagemPrincipal = new PrintWriter(socket.getOutputStream());
        mensagemPrincipal.println("Letras já usadas: " + letrasUsadas  + " | "
            + "Valendo " + pontuacaoRodada + " pontos, digitem uma letra.");
        mensagemPrincipal.flush();

        BufferedReader todoOuvidos = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        String letra = todoOuvidos.readLine();

        if (letrasUsadas.contains(letra)) {
            System.out.println("Essa letra já foi falada!!!");
            System.out.println();
            jogada(jogador);
        } else {
            letrasUsadas += letra + ", ";
            boolean contem = contemLetra(listaPalavras, letra);

        if (contem) {
            listaLetrasCertas.add(letra);
            System.out.println(listaLetrasCertas);
            leitor.mostrarLetras(listaPalavras, listaLetrasCertas);
            listaLetrasCertas = new ArrayList<>();

            jogador.setPontuacao(jogador.getPontuacao() + pontuacaoRodada);

            System.out.println("Pontuação Jogador " + jogador.getId() + ": " + jogador.getPontuacao());
            System.out.println();
            if (jogador.getPontuacao() > PONTUACAO_MAXIMA || listaLetrasCertas.contains(letrasUsadas)) {
                fimDeJogo(jogador);
            }
            jogada(jogador);
        } else {
            System.out.println("ERRRRRROUUUUU");
            System.out.println();
        }
      }
    }

    private static void printaRanking() {
        System.out.println();
        System.out.println("RANKING: ");
        System.out.println("NICKNAME   |   PONTUAÇÃO");
        for(Jogador j : listaJogadores) {
            System.out.println(j.getNome() + ":           " + j.getPontuacao());
        }
        System.out.println();
    }

    private static boolean contemLetra(List<String> palavras, String letra) {
        boolean contem = false;
        int contador = 0;
        for (String s : palavras) {
            String[] palavra = s.split("");
            for (String letraAtual : palavra) {
                if (letraAtual.equals(letra)) {
                    contador++;
                    contem = true;
                }
            }
        }
        System.out.println("Existem " + contador + " letras " + letra);
        return contem;
    }

    public static void fimDeJogo(Jogador jogador) throws IOException {
        PrintWriter fim = new PrintWriter(socket.getOutputStream());
        fim.println("***** FIM DE JOGO!!!!! | VITÓRIA DE " + jogador.getNome() + "!!!!!!!!!!!");
        fim.flush();
        jogando = false;
        System.exit(0);
    }

    private static void gerarPontuacaoRodada() {
        Random random = new Random();
        pontuacaoRodada = random.nextInt(250);
    }
}
