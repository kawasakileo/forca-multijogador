package forca.cliente;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Cliente {

    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("localhost", 7777);

        Scanner scanner = new Scanner(System.in);

        System.out.println("Bem vindo jogador!");

        InputStream input = socket.getInputStream();
        OutputStream output = socket.getOutputStream();

        BufferedReader in = new BufferedReader(new InputStreamReader(input));
        PrintStream out = new PrintStream(output);

        while (true) {
            System.out.print(in.readLine());
            System.out.println();

            String mensagem = scanner.nextLine();
            out.println(mensagem);

            if(mensagem.equals("SAIR") || mensagem.equals("sair")) {
                break;
            }
        }
    }
}
