package leitor;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Leitor {

    private final List<String> lista = new java.util.ArrayList<>(Collections.emptyList());

    public void lerArquivo(String nomeArquivo) {
        try {
            FileReader arquivo = new FileReader(nomeArquivo);
            BufferedReader lerArquivo = new BufferedReader(arquivo);

            String linha = lerArquivo.readLine();
            while (linha != null) {
                // Printa todas as linhas:
                // System.out.printf("%s\n", linha);
                linha = lerArquivo.readLine();
                lista.add(linha);
            }
            arquivo.close();
            lerArquivo.close();
        } catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.\n", e.getMessage());
        }
    }

    public String selecionarPalavraAleatoria() {
        String palavra = null;
        Random random = new Random();
        for (int i = 0 ; i < lista.size() - 1; i++) {
            palavra = String.valueOf(lista.get(random.nextInt(lista.size() - 1)));
        }
        return palavra;
    }

    public List<String> verificaPalavras() {
        String palavra = selecionarPalavraAleatoria();
        return Collections.singletonList(palavra);
    }

    public void printPalavrasSombreadas(List<String> palavras) {
        for (String s : palavras) {
            String[] palavra = s.split("");
            for (int j = 0; j < palavra.length; j++) {
                System.out.print("_ ");
            }
            System.out.println();
        }
    }

    public void mostrarLetras(List<String> palavras, List<String> letrasCertas) {
        for (String s : palavras) {
            String[] palavra = s.split("");
            for (String letraPalavra : palavra) {
                for (String letraCerta : letrasCertas) {
                    if (letraPalavra.equals(letraCerta)) {
                        System.out.print(letraCerta + " ");
                    } else {
                        System.out.print("_ ");
                    }
                }
            }
            System.out.println();
        }
    }
}
