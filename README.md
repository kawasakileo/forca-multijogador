# forca-multijogador

Projeto da matéria de Sistemas Operacionais.

Alunos: Leonardo Kawasaki e Leonardo Lima

## Como rodar 

Para rodar o jogo basta: 
* abrir duas abas de seu terminal 
* acessar o diretório deste código
* digitar primeiro o comando java -jar forca-multijogador.jar
* e então digitar o comando java -jar cliente.jar

Ou então:
* rodar via intellij, primeiramente a classe Servidor.java
* e rodar então a classe Cliente.java

obs: é necessário ter a versão JDK 13 do Java.
